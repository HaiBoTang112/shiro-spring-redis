$(function() {
	loginForm();
	init();
	rememberMeClick();
});

function init() {
	FastClick.attach(document.body);
}

function loginForm() {
	$.validator.addMethod("mobilePhone", function(value, element) {
		return /^1[35678]\d{9}$/.test(value);
	});
	$('#loginForm').validate({
        rules: {
        	mobilePhone: {
        		required: true,
        		mobilePhone: true
        	},
            password: {
            	required: true,
            	minlength: 6
            }
        },
        highlight: function(element, errorClass, validClass) {
        	$(element).next('span.input-end-icon').removeClass('success');
       	 	$(element).next('span.input-end-icon').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
	    	 $(element).next('span.input-end-icon').removeClass('error');
	     	 $(element).next('span.input-end-icon').addClass('success');
        },
        errorPlacement: function(error, element) {
            //element.attr("placeholder",error.text());
         }
    });

    $('#loginForm input').on('keyup blur', function () {
        if ($('#loginForm').valid()) {
            $('.login-btn').addClass('enabled');
        } else {
            $('.login-btn').removeClass('enabled');
        }
    });
    
    $('body').delegate(".login-btn", "click", function() {
		if($(this).hasClass('enabled') && $('#loginForm').valid()) {
			$(this).removeClass('enabled');
			$("#loginForm").submit();
		}
	});
}

function rememberMeClick() {
	$(".remember-me-container").click(function(){
		var chk = $(this).find(".checkbox");
		chk.toggleClass("actived");
		chk.siblings('input[name="remember"]').val("false");
		if(chk.hasClass('actived')) {
			chk.siblings('input[name="remember"]').val("true");
		}
	});
}

